package models

// DownloadJob struct defines the data model to hold a download job object.
type DownloadJob struct {
	VideoID         string
	VideoTitle      string
	URL             string
	FilePath        string
	FileExtSet      bool
	FileSize        string
	Priority        int
	State           string
	StateInfo       string
	StateHasChanged bool
}
