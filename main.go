package main

import (
	"fmt"
	"time"

	"gitlab.com/staticvoidlabs/cpsgo/manager"
	"gitlab.com/staticvoidlabs/cpsgo/models"
)

var mShouldExit = false
var mCurrentConfig models.Config

func main() {

	// Init config.
	mCurrentConfig = manager.GetCurrentConfig()

	fmt.Println(" CouchPlayerServer started. (Version " + manager.GetVersionInfo() + ")")
	fmt.Println("")

	// Initialite api service.
	manager.InitAPIService()

	// Enter the main loop.
	for !mShouldExit {

		// Update shows repo.
		manager.InitUpdateShowsRepo()

		time.Sleep(1 * time.Second)
	}

}
