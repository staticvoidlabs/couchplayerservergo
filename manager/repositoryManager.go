package manager

import (
	"time"

	"gitlab.com/staticvoidlabs/cpsgo/models"
)

var showsRepo models.ShowsRepo
var isUpdating bool = false

// InitUpdateShowsRepo inits and updates the show repo.
func InitUpdateShowsRepo() {

	if !isUpdating {

		isUpdating = true

		showsRepo.Shows = nil

		showsRepo.DateLastUpdate = time.Now()

		isUpdating = false

	}

}
