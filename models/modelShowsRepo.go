package models

import (
	"time"
)

// struct and funcs for repo model.
type ShowsRepo struct {
	Shows          []ShowItem
	DateLastUpdate time.Time
}

func (s ShowsRepo) Len() int {
	return len(s.Shows)
}

//--------------------------------------------

// struct and funcs for show model.
type ShowItem struct {
	ShowID          int
	FullName        string
	Title           string
	TitleShort      string
	TitlDetail      string
	Origin          string
	OriginShort     string
	OriginLogoScale float32
	Duration        int
	AgeInDays       int
	FullPath        string
	Thumbnail       string
	DateAdded       time.Time
	DateBroadcast   string
	Format          string
}

func (s *ShowItem) SetAgeInDays() {
	s.AgeInDays = int(time.Now().Sub(s.DateAdded).Hours()/24) + 1
}
