package manager

import (
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

// InitAPIService starts the API service.
func InitAPIService() {

	// Create router instance.
	router := mux.NewRouter().StrictSlash(true)

	// Define routes and actions.
	router.HandleFunc("/", getStateInfoService)
	router.HandleFunc("/cps/shows", generateHTMLOutput)
	//router.PathPrefix("/nappy/news").Handler(http.StripPrefix("/nappy/news", http.FileServer(http.Dir("./www"))))

	// Start rest service.
	go log.Fatal(http.ListenAndServe(":9100", router))

}

// Endpoint implementation.
func getStateInfoService(w http.ResponseWriter, r *http.Request) {

	json.NewEncoder(w).Encode("getStateInfoService")
}

func generateHTMLOutput(w http.ResponseWriter, r *http.Request) {

	// Try to parse template.
	tmpl, err := template.ParseFiles("./www/template_news_simple.html")
	if err != nil {
		fmt.Println("Error parsing template: ", err)
	}

	err = tmpl.Execute(w, mDownloadQueue)
	if err != nil {
		fmt.Println("Error executing template: ", err)
	}

	//json.NewEncoder(w).Encode("VoidServices_response:generate_HTML_Output:success.")
}

func htmlTesting(w http.ResponseWriter, r *http.Request) {

	//json.NewEncoder(w).Encode("<p>Regards,<br/>Dark Universe Data Lake, managed by Crimson Macaw Wormhole</p>`")
	fmt.Print("<p>Regards,<br/>Dark Universe Data Lake, managed by Crimson Macaw Wormhole</p>`")
}
